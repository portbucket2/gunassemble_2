﻿using UnityEngine;
using TMPro;

public class FPSDisplay : MonoBehaviour
{
	public TextMeshProUGUI currentFPS, lowestFPS, milisecond;

	float deltaTime = 0.0f;
	float lowestfps = 500f;

	void Update()
	{
		deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
		OnDisplay();
	}


	float msec;
	float fps;
	void OnDisplay()
	{
		msec = deltaTime * 1000.0f;
		fps = 1.0f / deltaTime;

		currentFPS.text = "CurrentFps: " + Mathf.Ceil(fps);
		lowestfps = fps < lowestfps ? fps : lowestfps;
		lowestFPS.text = "LowestFps: " + Mathf.Ceil(lowestfps);
		milisecond.text = "Mili-Second: " + Mathf.Ceil(msec);

	}
}