﻿using TMPro;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GunAsset gunAsset;
    public Transform asimulatedGunHolder, assemblePosition, assembleCamPosition, initialCamPosition, leftHandObj;
    public Material vertexMaterial, fadeMaterial;
    public float distanceMagnitude = 2f;
    public float snappingDistance = 0.35f;
    public float deassambleTime = 0.3f;
    
    public int totalPartsCounter;
    public int totalAssembledCounter;

    public bool isPlayMode;

    Vector3 shootingShakeInitialPosition, shootingShakeInitialRotation;
    Transform leftHandPosition;
    private void Awake()
    {
        PlayerPrefs.DeleteKey("CurrentLevel");
        instance = this;
        DOTween.Init();
        shootingShakeInitialPosition = shakeAnim.transform.position;
        shootingShakeInitialRotation = shakeAnim.transform.eulerAngles;

        PlayNextLevel();
    }

    public void PlayNextLevel()
    {
        isPlayMode = false;
        totalPartsCounter = 0;
        totalAssembledCounter = 0;
        assemblePanel.SetActive(false);
        panelColor.SetActive(false);
        panelShooting.SetActive(false);
        panelShowCase.SetActive(false);

        foreach (Transform item in asimulatedGunHolder)
        {
            Destroy(item.gameObject);
        }
        foreach (Transform item in assembleTaskShowPanel)
        {
            Destroy(item.gameObject);
        }
        foreach (Transform item in paintTaskShowPanel)
        {
            Destroy(item.gameObject);
        }
        asimulatedGunHolder.parent = assemblePosition;
        asimulatedGunHolder.localPosition = Vector3.zero;
        asimulatedGunHolder.localEulerAngles = new Vector3(0, 90, 90);
        asimulatedGunHolder.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        //asimulatedGunHolder.localScale = Vector3.one;

        shooting.gunAnim.SetTrigger("HandDown");
        shakeAnim.enabled = false;
        shooting.enabled = false;
        Camera.main.transform.DOMove(initialCamPosition.position, deassambleTime);
        Camera.main.transform.DORotate(initialCamPosition.eulerAngles, deassambleTime).OnComplete(()=> {

            LoadCurrentLevel();

        });

    }

    int currentLevel;
    private void LoadCurrentLevel()
    {
        currentLevel = PlayerPrefs.GetInt("CurrentLevel", 0);

        int maxLevel = 0;
        for (int i = 0; i < gunAsset.gunCollection.Length; i++)
        {
            maxLevel += gunAsset.gunCollection[i].allGuns.Length;
        }

        if (currentLevel >= maxLevel)
        {

            Debug.Log("No more gun. Starting game from First");

            PlayerPrefs.SetInt("CurrentLevel", 0);
            currentLevel = 0;
        }
        PlayerPrefs.SetInt("CurrentLevel", PlayerPrefs.GetInt("CurrentLevel") + 1);

        int index = currentLevel;
        for (int i = 0; i < gunAsset.gunCollection.Length; i++)
        {
            if(index < gunAsset.gunCollection[i].allGuns.Length)
            {
                // Here is exist current level
                currentLodedGun = gunAsset.gunCollection[i].allGuns[index];
            }
            else
            {

                index -= gunAsset.gunCollection[i].allGuns.Length;
            }
        }


        StartCoroutine (DeAssembleGun());
    }

    Transform currentLodedGun, deassembleGun, assembleGun;
    IEnumerator DeAssembleGun()
    {
        Vector3 pos;

        #region Assaign Materials
        assembleGun = Instantiate(currentLodedGun, asimulatedGunHolder);
        assembleGun.localPosition = Vector3.zero;
        assembleGun.localEulerAngles = Vector3.zero;
        foreach (Transform gunParts in assembleGun)
        {
            if (gunParts.name.Equals("LHP"))
                continue;

            gunParts.GetComponent<MeshRenderer>().material = fadeMaterial;
            totalPartsCounter++;
        }
        //assembleGun.GetComponent<MeshRenderer>().material = fadeMaterial;

        deassembleGun = Instantiate(currentLodedGun, asimulatedGunHolder);
        deassembleGun.localPosition = Vector3.zero;
        deassembleGun.localEulerAngles = Vector3.zero;
        foreach (Transform gunParts in deassembleGun)
        {
            if (gunParts.name.Equals("LHP"))
            {
                leftHandPosition = gunParts;
                continue;
            }

            gunParts.gameObject.GetComponent<MeshRenderer>().material = vertexMaterial;
        }
        //deassembleGun.GetComponent<MeshRenderer>().material = vertexMaterial;
        #endregion
         
        #region Deassemble Gun by Animation
        int totalObjectCount = deassembleGun.childCount + 1;
        float angle = 360 / totalObjectCount * 10;
        foreach (Transform gunParts in deassembleGun)
        {
            if (gunParts.name.Equals("LHP"))
            {
                continue;
            }

            gunParts.gameObject.AddComponent<DesignDragger>().SetParent(deassembleGun);
            MeshCollider partHolder = gunParts.gameObject.AddComponent<MeshCollider>();
            partHolder.sharedMesh = gunParts.gameObject.GetComponent<MeshFilter>().mesh;
            pos = Random.insideUnitCircle.normalized * distanceMagnitude;
            pos += deassembleGun.position;
            pos.z = pos.y;
            pos.y = gunParts.position.y;
            gunParts.DOMove(pos, deassambleTime).OnComplete(()=> {

                gunParts.localPosition = new Vector3(0, gunParts.localPosition.y, gunParts.localPosition.z);
            });
            yield return new WaitForSeconds(deassambleTime);
        }
        int loopCount = 5;
        while (deassembleGun.childCount > 0)
        {
            foreach (Transform gunParts in deassembleGun)
            {
                gunParts.parent = asimulatedGunHolder;
            }

            loopCount--;
            if (loopCount <= 0)
                break;
        }
        #endregion


        Camera.main.transform.DOMove(assembleCamPosition.position, deassambleTime);
        Camera.main.transform.DORotate(assembleCamPosition.eulerAngles, deassambleTime).OnComplete(() => {


            ActiveAssemble();
            isPlayMode = true;

        });
    }

    List<TaskImage> assembleTaskImages;
    public void OnAssembleAParts()
    {
        assembleTaskImages[totalAssembledCounter].SetCompleted();
        totalAssembledCounter++;

        if (totalAssembledCounter >= totalPartsCounter)
        {

            Invoke("ActivatePainting", 1);
        }

    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
            ActivatePainting();

        if (Input.GetKey(KeyCode.Escape))
            Application.Quit();
    }

    [Header("Assemble")]
    public Transform assembleTaskShowPanel;
    public GameObject assemblePanel, taskImagePrefab;
    public void ActiveAssemble()
    {
        assembleTaskImages = new List<TaskImage>();
        for (int i = 0; i < totalPartsCounter; i++)
        {
            assembleTaskImages.Add(Instantiate(taskImagePrefab, assembleTaskShowPanel).GetComponent<TaskImage>());
        }

        assemblePanel.SetActive(true);
    }

    [Header("Paint")]
    public GameObject panelColor;
    public GameObject starPrefab;
    public Transform paintTaskShowPanel, camPosition, gunPosition, painter;
    public Rotator rotator;
    List<TaskImage> paintTaskImages;
    private void ActivatePainting()
    {
        assemblePanel.SetActive(false);
        paintTaskImages = new List<TaskImage>();
        colorTouchCount = 0;
        for (int i = 0; i < totalPartsCounter; i++)
        {
            paintTaskImages.Add(Instantiate(starPrefab, paintTaskShowPanel).GetComponent<TaskImage>());
        }
        assembleGun.gameObject.SetActive(false);
        Camera.main.transform.DOMove(camPosition.position, deassambleTime);
        Camera.main.transform.DOLookAt(gunPosition.position, deassambleTime);
        //Camera.main.transform.DORotate(gunPosition.position, deassambleTime);

        asimulatedGunHolder.DORotate(new Vector3(0, 0, 0), deassambleTime);
        asimulatedGunHolder.DOMove(gunPosition.position, deassambleTime).OnComplete(()=> {

            rotator.enabled = true;
            panelColor.SetActive(true);
            painter.gameObject.SetActive(true);

        });
    }

    int colorTouchCount;
    public void OnColorTouchInFreshMesh()
    {
        paintTaskImages[colorTouchCount++].SetCompleted();
    }

    public void OnBrushSizeChange(float value)
    {
        painter.GetComponent<Painter>().brushSize = value;
    }

    [Header("Shoot")]
    public GameObject panelShooting;
    public Animator shakeAnim, scoreAnim;
    public Transform shootingCamPos, shootingGunPos;
    public TextMeshProUGUI pointsText, bulletText, scoreShowText;
    int totalShootingValue = 0, totalRemainingBullets = 5;
    public void ActivateShooting()
    {
        shakeAnim.transform.position = shootingShakeInitialPosition;
        shakeAnim.transform.eulerAngles = shootingShakeInitialRotation;
        totalShootingValue = 0;
        totalRemainingBullets = 5;
        SetShootPanelValue(0);
        painter.gameObject.SetActive(false);
        rotator.enabled = false;
        panelColor.SetActive(false);
        Camera.main.transform.DOMove(shootingCamPos.position, deassambleTime);
        Camera.main.transform.DORotate(shootingCamPos.eulerAngles, deassambleTime).OnComplete(() => {

            asimulatedGunHolder.parent = shootingGunPos;
            asimulatedGunHolder.localPosition = Vector3.zero;
            asimulatedGunHolder.localEulerAngles = new Vector3(0, 0, 0);
            asimulatedGunHolder.localScale = Vector3.one;
            leftHandObj.position = leftHandPosition.position;

            panelShooting.SetActive(true);
            shooting.enabled = true;
            shakeAnim.enabled = true;
        });

    }

    public Shooting shooting;
    public void SetShootPanelValue(int shootingValue)
    {
        totalShootingValue += shootingValue;
        totalRemainingBullets--;
        scoreShowText.text = "";
        if (shootingValue != 0)
        {
            scoreShowText.text = shootingValue == 25 ? "BullsEye\n+" + shootingValue : "+" + shootingValue;
            scoreAnim.SetTrigger("ShowPoints");
        }
        pointsText.text = totalShootingValue.ToString();
        //bulletText.text = "Bullets: " + totalRemainingBullets;

        if (totalRemainingBullets <= 0)
        {
            isPlayMode = false;
            Invoke("ActivateShowCase", 1);
        }
    }


    public GameObject panelShowCase;
    public Transform showCaseGunHolder, showCaseCamPosition;
    public void ActivateShowCase()
    {
        panelShooting.SetActive(false);
        shooting.enabled = false;
        shakeAnim.enabled = false;

        Camera.main.transform.DOMove(showCaseCamPosition.position, deassambleTime);
        Camera.main.transform.DORotate(showCaseCamPosition.eulerAngles, deassambleTime).OnComplete(() => {

            Camera.main.transform.DORotate(showCaseCamPosition.eulerAngles, deassambleTime).OnComplete(() => {

                shooting.gunAnim.SetTrigger("Shoot");
                Transform showCaseItem = asimulatedGunHolder.GetChild(1);
                showCaseItem.parent = showCaseGunHolder.GetChild(currentLevel);

                showCaseItem.DOMove(showCaseGunHolder.GetChild(currentLevel).position, deassambleTime);
                showCaseItem.DORotate(new Vector3(45, 0, 0), deassambleTime).OnComplete(() => {

                    shooting.gunAnim.SetTrigger("HandDown");
                    showCaseGunHolder.GetChild(currentLevel).GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = totalShootingValue + "/100";

                    showCaseItem.DORotate(new Vector3(45, 0, 0), deassambleTime).OnComplete(() => {

                        panelShowCase.SetActive(true);


                    });

                });

            });

        });

    }

}
