﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Painter : MonoBehaviour
{
    public Color color;
    public Vector3 paintOffest;
    public Transform sprayBottle, cameraHolder;
    public Material lineMaterial;
    public float brushSize = 0.1f;
    public float detectingSize = 0.1f;

    Color[] c;
    Vector3 initialPosition;
    private void Awake()
    {
        //OnColorSelect(image);
        color = lineMaterial.color;
    }

    private void OnEnable()
    {
        initialPosition = cameraHolder.position;
    }

    private void OnDisable()
    {
        sprayBottle.gameObject.SetActive(false);
    }

    public void OnColorSelect(Image image)
    {
        color = image.color;
        lineMaterial.SetColor("_Color", color);

    }

    Vector3 lastMousePos;
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            cameraHolder.DOMove( initialPosition, GameManager.instance.deassambleTime);
        }
        //Camera.main.transform.LookAt(GameManager.instance.gunPosition);
        sprayBottle.gameObject.SetActive(Input.GetMouseButton(0));


        if (Input.GetMouseButtonDown(0))
        {
            lastMousePos = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {

            #region Camera gun axis move
            //Vector3 pos = cameraHolder.position;
            //pos += new Vector3(0, (Input.mousePosition.y - lastMousePos.y) * Time.deltaTime, 0);
            //pos = Vector3.Lerp(cameraHolder.position, pos, 1f);
            //if (Vector3.Distance(initialPosition, pos) < 3)
            //{
            //    cameraHolder.position = pos;
            //}

            #endregion

            Vector3 bottlePos = GetWorldPositionOnPlane(Input.mousePosition + paintOffest, 0);
            sprayBottle.DOLookAt(bottlePos, 0.1f);

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition + paintOffest);

            if (!Physics.Raycast(ray, out hit, 100))
                return;
            
            MeshCollider meshCollider = hit.collider as MeshCollider;
            if (meshCollider == null || meshCollider.sharedMesh == null)
                return;

            bottlePos = hit.point;

            //PaintAsDump(hit);
            PaintAsPro(hit);

            sprayBottle.LookAt(bottlePos);
        }
    }

    void PaintAsPro(RaycastHit hit)
    {
        Mesh mesh = hit.collider.GetComponent<MeshFilter>().mesh;
        Vector3[] vertpos = mesh.vertices;
        Color[] meshcolors = mesh.colors;
        if (meshcolors.Length == 0)
        {
            Debug.Log("On color touch new mesh.");
            GameManager.instance.OnColorTouchInFreshMesh();
            meshcolors = new Color[mesh.vertices.Length];
            for (int i = 0; i < meshcolors.Length; i++)
            {
                meshcolors[i] = Color.white;
            }
        }

        for (int i = 0; i < vertpos.Length; i++)
        {
            if ((hit.point - hit.transform.TransformPoint(vertpos[i])).sqrMagnitude < brushSize)
            {
                meshcolors[i] = color;
            }
        }
        mesh.colors = meshcolors;
    }

    void PaintAsDump(RaycastHit hit)
    {
        Mesh mesh = hit.collider.GetComponent<MeshFilter>().mesh;

        int[] triangles = mesh.triangles;
        c = mesh.colors;

        if (c.Length == 0)
        {
            Debug.Log("On color touch new mesh.");
            GameManager.instance.OnColorTouchInFreshMesh();
            c = new Color[mesh.vertices.Length];
            for (int i = 0; i < c.Length; i++)
            {
                c[i] = Color.white;
            }
        }

        c[triangles[hit.triangleIndex * 3]] = color;
        c[triangles[hit.triangleIndex * 3 + 1]] = color;
        c[triangles[hit.triangleIndex * 3 + 2]] = color;
        for (int i = 0; i < brushSize; i++)
        {
            if ((hit.triangleIndex - i) >= 0)
            {
                c[triangles[(hit.triangleIndex - i) * 3]] = color;
                c[triangles[(hit.triangleIndex - i) * 3 + 1]] = color;
                c[triangles[(hit.triangleIndex - i) * 3 + 2]] = color;
            }

            if (((hit.triangleIndex + i) * 3 + 2) < triangles.Length)
            {
                c[triangles[(hit.triangleIndex + i) * 3]] = color;
                c[triangles[(hit.triangleIndex + i) * 3 + 1]] = color;
                c[triangles[(hit.triangleIndex + i) * 3 + 2]] = color;
            }
        }

        mesh.colors = c;
    }

    private void LateUpdate()
    {
        lastMousePos = Input.mousePosition;
    }

    public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenPosition);
        Plane xy = new Plane(Camera.main.transform.forward, Vector3.zero);
        float distance;
        xy.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
}
