﻿using TMPro;
using UnityEngine;

public class FPSCalculator : MonoBehaviour
{
    public TextMeshProUGUI currentFPSText, lowestFPSText;

    float deltaTime;
    float lowestFPS;

    private void Awake() {

        lowestFPS = 60f;
    }

    // Update is called once per frame
    void Update()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        float fps = 1.0f / deltaTime;

        if (fps < lowestFPS)
            lowestFPS = fps;

        currentFPSText.text = "Current FPS: " + Mathf.Ceil(fps).ToString();
        lowestFPSText.text = "Lowest FPS: " + Mathf.Ceil(lowestFPS).ToString();
    }
}
