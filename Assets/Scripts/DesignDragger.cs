﻿using UnityEngine;

public class DesignDragger : MonoBehaviour
{

    Transform parent;
    Vector3 initialPos;
    bool isPlaced = false;
    private void Awake()
    {
        initialPos = transform.localPosition;
    }

    public void SetParent(Transform obj)
    {
        parent = obj;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1) && !isPlaced)
            FinishThisDrag();
    }

    private void OnMouseDrag()
    {
        if (!isPlaced && GameManager.instance.isPlayMode)
        {

            Vector3 pos = GetWorldPositionOnPlane(Input.mousePosition, transform.position.z);
            //pos.z = pos.y;
            pos.y = transform.position.y;
            transform.position = pos;

            if (Vector3.Distance(transform.localPosition, initialPos) <= GameManager.instance.snappingDistance)
            {
                FinishThisDrag();
            }
        }
    }

    public void FinishThisDrag()
    {
        isPlaced = true;
        transform.parent = parent;
        transform.localPosition = initialPos;
        //transform.localPosition = new Vector3(0, transform.localPosition.y, transform.localPosition.z);
        GameManager.instance.OnAssembleAParts();
        enabled = false;

    }

    public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenPosition);
        Plane xy = new Plane(Camera.main.transform.forward, transform.position);
        float distance;
        xy.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
}