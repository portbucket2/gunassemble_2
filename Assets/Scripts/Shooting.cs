﻿using UnityEngine;

public class Shooting : MonoBehaviour
{
    public Animator gunAnim;
    RaycastHit hit;

    private void OnEnable()
    {
        gunAnim.SetTrigger("HandUp");
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && GameManager.instance.isPlayMode)
        {
            gunAnim.SetTrigger("Shoot");
            if (Physics.Raycast(new Ray(Camera.main.transform.position, Camera.main.transform.forward), out hit))
            {
                GameManager.instance.SetShootPanelValue(int.Parse(hit.collider.name));

            }
        }
    }
}
