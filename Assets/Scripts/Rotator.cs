﻿using UnityEngine;

public class Rotator : MonoBehaviour
{
    public Vector3 speed;
    private void FixedUpdate()
    {
        transform.localEulerAngles += speed * Time.fixedDeltaTime;
    }
}
