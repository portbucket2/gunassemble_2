﻿using UnityEngine.UI;
using UnityEngine;

public class TaskImage : MonoBehaviour
{
    public Image backgroundImage, completeImage;
    public Sprite bgSprite, completeSprite;

    private void Awake()
    {
        backgroundImage.sprite = bgSprite;
        completeImage.sprite = bgSprite;
    }

    public void SetCompleted()
    {
        completeImage.sprite = completeSprite;
    }
}
